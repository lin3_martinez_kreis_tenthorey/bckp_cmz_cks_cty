---
title: BCKP - Rapport
author: Cédric Martinez
geometry: margin=2cm,a4paper
fontsize: 12pt
fontfamily: avant
lang: french
toccolor: black
numbersections: yes
header-includes:
- \usepackage{fancyhdr}
- \usepackage{xcolor}
- \usepackage{hyperref}
- \usepackage{titling}
- \usepackage[T1]{fontenc}
- \usepackage{lmodern}
- \usepackage{lastpage}
- \usepackage{ragged2e}
- \hypersetup{colorlinks = true,linkcolor = black}
- \pagestyle{fancy}
- \fancyhead[LO,LE]{CPNV}
- \fancyfoot[C]{Page \char58 \ \thepage \ sur \pageref{LastPage}}
- \fancyfoot[RO,LE]{Créé le \char58 \  \today}
- \fancyfoot[RE,LO]{Auteur \char58 \ Cédric Martinez}
link-citations: true
nocite: |
  @*
---
\centering
![Logo](..\Figures\I-BCKP-CMZ-LogoBackupExec.jpg)
\newpage

\raggedright
\tableofcontents

\newpage
\justify

# Introduction

Le but du module Backup consiste à mettre en place la politique de sauvegarde da la section Médiamatique de l'école CPNV de la manière la plus optimale possible et respectant au mieux les Best Pratices. Le présent document détaille la réflexion ainsi que les raisons qui ont menés au choix de la politique de sauvegarde présentée. Le tout sera complété par une présentation.

# Analyse

## Produit utilisé

Le produit utilisé sera BackupExec 2016 comme indiqué dans le cahier des charges.

## Données à sauvegarder

Selon le cahier des charges, il est fait état de 120 postes fixes ayant chacun 2GB de données à sauvegarder, ainsi que 30 portables ayant chacun 4 GB de données à sauvegarder ainsi qu'un serveur ayant quatres partages à sauvegarder c'est à dire Commun classe d'une taille de 3 GB, Commun Profs Média de 20 GB, Perso élèves d'une taille de 2 GB par élève et Perso profs de 5 Gb par enseignant.

Plusieurs sources indiquent que le taux de changement entre les sauvegardes se situe entre 5 et 10%, afin de garentir une marge de sécurité, il a été décidé de prendre en compte une marge de 10% de modification des données entre chaque sauvegarde incrémentielle et complète par rapport au volume total.

### Postes fixe

Pour les postes fixes cela représente un total de 120 * 2 GB = 240 GB de données à sauvegarder le tout plus une marge de sécurité estimant une modification de 10% des données par sauvegarde ce qui représenterait 240+(240/100*5)= 319,9 GB au bout d'une année.

### Ordinateurs portables

Pour les ordinateurs portables cela représente un total de 30 * 4 GB = 120 GB de données à sauvegarder le tout plus une marge de sécurité estimant une modification de 10% des données par sauvegarde ce qui représenterait 120+(120/100*5)= 159,9 GB au bout d'une année.

### Partages

Pour le partage Commun classe cela représente un total 3 GB par classe sachant qu'il y a 30 classes de médiamatiques = 3 * 30 = 90 GB de données à sauvegarder le tout plus une marge de sécurité une modification de 5% des données par sauvegarde ce qui représenterait 90+(90/100*5)= 119,9 GB au bout d'une année.

Pour le partage Commun profs cela représente 20 GB de données à sauvegarder le tout plus une marge de sécurité estimant une modification de 10% des données par sauvegarde ce qui représenterait 20+(20/100*5)= 26,6 GB au bout d'une année.

Pour le partage Perso élève cela représente un total de 2 GB par élève sachant qu'il y a 302 élèves de médiamatiques = 2 * 302 = 604 GB de données à sauvegarder le tout plus une marge de sécurité estimant une modification de 10% des données par sauvegarde ce qui représenterait 604+(604/100*5)= 805,3 GB au bout d'une année.

Pour le partage Perso profs cela représente un total de 5 GB par enseignant sachant qu'il y a environ 104 enseignants en médiamatique = 5 * 104 = 520 GB de données à sauvegarder le tout plus une marge de sécurité estimant une modification de 10% des données par sauvegarde ce qui représenterait 520+(520/100*5)=693,3 GB au bout d'une année.

Pour une sauvegarde complete, cela représente au minimum un total de 240+120+90+20+604+520 = 1594 GB sans compter l'augmentation au bout d'une année.

En utilisant, la vitesse moyenne de lecture d'un disque dur SATA 7200RPM de 80 mbps la sauvegarde prendrait dans ces conditions théoriques 1 jour, 20 heures et 16 minutes à être transférée. ^[Voir http://wintelguy.com/transfertimecalc.pl]

Ce temps de transfert dépasse la fenêtre de sauvegarde prévue entre 22 heures et 6 heures du matin qui est de 8 heures. Pour ne pas dépasser cette fenêtre, il serait nécessaire d'avoir une bande passante minimale de 443 mbps dans ces conditions la sauvegarde prendrait 7 heures et 59 minutes ce qui laisse trop peu de marge de sécurité.

Afin de disposer d'une heure de marge, il faudrait disposer d'une bande passante de 505 mbps qui permetterait à la sauvegarde de s'effectuer en 7h.

Afin de garentir une marge de sécurité, il semble primordial de disposer de l'espace de stockage nécessaire pour 5 ans de sauvegarde en prenant en compte 5% d'augumentation par année ce qui représenterait (240+((240/100 * 10) * 5))+(120+((120/100* 10)* 5))+(90+((90/100* 10)* 5))+(20+((20/100* 10)* 5))+(604+((604/100* 10)* 5))+(520+((520/100* 10)* 5))=(1039,9)+(519,9)+(389,9)+(86,6)+(2617,3)+(2253,3)=6906,93 GB de données à sauvegarder.
+
La sauvegarde de 6905 GB prendrait 1 jours 10 heures et 38 minutes en utilisant une bande passante de 443 mbps ce qui dépasse la fenêtre de sauvegarde.
Il faut donc disposer d'une plus grand ebande que celle strictement nécessaire actuellement afin de ne pas dépasser dans le futur la fenêtre de sauvegarde.
Afin d'effectuer la sauvegarde en 8h il est nécessaire de disposer d'une bande passante de 1920 mbps ce qui nécessite de disposer d'une carte réseau 1gbps disposant de deux ports configuré en mode d'aggregation.
Afin d'effectuer la sauvegarde en 7h, il est nécessaire de disposer d'une bande passante minimale de 2190 mbps ce qui nécessite soit trois ports 1gbps soit une carte réseau 10gbps.
Avec une telle carte la sauvegarde s'effectuerait dans les meilleures conditions en 1 heure et 32 minutes environ.

Sachant qu'une augmentation du traffix exponentielle est probable en 5 ans, il est recommandé d'équiper les hyperviseurs de cartes réseaux 10gbps comme par example les cartes Intel X710-T4 à 598 USD pièce soit 562 CHF environ ^[Voir https://ark.intel.com/products/93428/Intel-Ethernet-Converged-Network-Adapter-X710-T4] si ce n'est pas déjà le cas et de configurer les machines virtuelles qui hebergeront les serveurs de sauvegarde avec deux cartes 10gbps.

\newpage

## Politique de sauvegarde

L'environnement est une école utilisant une infrastructure entièrement Microsoft, ce qui implique plusieurs facteurs à prendre en considération.

Tout d'abord, les données les plus importantes sont celles situées sur les partages Commun Classe, Commun Profs Média, Perso élèves et Perso Profs. En effet, ne pas disposer de ces données est un problème bloquant empêchant le bon déroulement des cours. Il est donc impératif de disposer d'une sauvegarde incrémentielle bijournalière de ces données sur disque conservéees deux semaines ainsi que d'une sauvegarde hebdomadaire complète conservée 4 semaines sur disque puis transférée sur tape une fois par semaine et conservée 8 semaines, une sauvegarde mensuelle complète sur disque conservée 3 mois puis transférée sur tape une fois par mois et conservée 1 année, puis une sauvegarde complète annuelle sur disque conservée 2 ans puis transférée sur tape chaque année et conservée 10 ans.

Viennent ensuite les données des postes fixes qui sont moins importantes que celles des partages puisqu'il n'est pas bloquant de ne pas en disposer. Si un disque dur dans un poste fixe devait ne pas fonctionner, l'étudiant peut utiliser un autre poste et utiliser les données stockées sur son partage Perso et sur le Commun Classe sans être impacté par la perte du poste si ce n'est par la perte de son profil ce qui peut se corriger en mettant en place des profils itinérents ce qui n'est pas le cas au CPNV. Une sauvegarde incrémentielle hebdomadaire sur disque conservée 6 semaines ainsi qu'une sauvegarde complète mensuelle sur disque conservée 3 mois, puis transférée sur tape et conservée 6 mois, puis une sauvegarde complète annuelle sur disque conservée 2 ans puis transféré sur tape et conservée 10 ans semble le plus adapté.

Les données des ordinateurs portables ne pouvant pas être forcément sauvegardées de manière fréquente, il convient d'effectuer des sauvegardes complètes chaque semaine.

## Flux de sauvegarde

Les données seront tout d'abord sauvegardées sur disque, puis sur des lecteurs de bande virtuels selon la stratégie Disk-to-Disk-to-Tape, le but étant de réduire le temps nécessaire à la sauvegarde effectuée durant la période 7h entre 22h et 6h du matin chaque nuit et effectuer la copie de la sauvegarde du disque vers les bandes la journée car cella a un impact moindre sur la bande passante des utilisateurs puisque cela est situé sur un autre réseau.^[Voir http://blog.open-e.com/d2d2t-a-disk-to-disk-to-tape-backup-strategy/]

\newpage

## Infrastructure envisagée

Afin de pouvoir répondre aux besoins de redondance, il a été décidé d'implémenter 4 serveurs Backup Exec sous la forme de machines virtuelles dans l'infrastructure virtuelle existante, un serveur Central Administration Server qui gérera les trois autres serveurs qui feront office de Managed Media Server ainsi que deux serveurs StarWind servant de bibliothèque de tape virtuelle.

Il existe différents types de licences pour Backup Exec qui sont Capacity, A la carte ou V-Ray.
Sachant que plusieurs serveurs Backup Exec seront utilisés, il semble plus avantageux de prendre des licences Capacity qui sont factuéres à la capacité non-compressée sauvegardée. Sachant que au minimum 1,25 TB seront utilisés et que les licences sont factuéres au TB et au serveur, l'achat de 4 licences Backup Exec Capacity Edition permet d'avoir une marge de sécurité. ^[Voir https://www.veritas.com/content/support/en_US/doc/ka6j000000009ubAAA-0]

Ces 4 licences représenteraient un coût de 36576,36 USD par an soit 34430 CHF par an. ^[Voir http://www.netsecuritystore.com/Symantec-Backup-Exec-Capacity-Edition.asp]

StarWind est décliné en différentes versions, les versions incluant les tapes virtuelles sont StarWind Virtual Tape Library et StarWind Virtual Tape Library Appliance.
Le prix n'étant pas indiqué sur le site Internet, il a fallu envoyer un e-mail afin d'obtenir des informations sur les prix des licences. Les anciens prix pour StarWind VTl étaient de 1795 USD par serveur ainsi que 359 USD par année par serveur. ^[Voir https://www.starwindsoftware.com/buy2] Ce qui représente ((1795*2)+(359*2))=3590+718=4308 USD soit environ 4050 CHF pour la première année et 718 USD soit 675 CHF pour les années suivantes.

Les deux licences nécessaires

Les machines virtuelles seront situées toutes dans des hypervieurs situés dans des batiments séparés les uns des autres.

La machine virtuelle servant de CAS disposera des spécifications suivantes:

| Composant | Spécification |
| :------------- | :------------- |
| Processeur | 2 cores |
| Mémoire Vive | 8 GB |
| Espace disque dur | 100 GB |
| Carte réseau | 2*1 Gbps |

Les trois machines virtuelles servant de MMS disposeront des spécifications suivantes:

| Composant | Spécification |
| :------------- | :------------- |
| Processeur | 1 core |
| Mémoire Vive   | 8 GB |
| Espace disque dur  | 50 GB |
| Carte réseau | 2*10 Gbps |

Le système d'exploitation qui sera mis en place sur les serveurs sera Windows Server 2016 standard puisqu'il s'agit de la version actuellement supportée par Microsoft et recommendée par Backup Exec.

L'espace disque pour la sauvegarde des données sera situé sur un SAN accessible de manière redondante depuis les hyperviseurs via iSCSI.

Un équipement approprié serait le SAN HPE MSA 1050 puiqu'il dispose d'interfaces 10Gbps et de deux contrôleurs pour la redondance. ^[Voir https://www.hpe.com/us/en/product-catalog/storage/disk-storage/pip.overview.hpe-msa-1050-10gbe-iscsi-dual-controller-lff-storage.1010291511.html]

Son prix est estimé à 8250 USD soit environ 7765 CHF. ^[Voir http://itprice.com/hp-price-list/q2r24a.html]

Il faut y ajouter l'achat de disques HPE MSA LFF, il en faut au minimum 6 de 2 TB à 584 USD pièce donc 3504 USD environ 3298 CHF. ^[Voir http://itprice.com/hp-price-list/n9x93a.html]
Mais le mieux serait de faire l'achat de deux SAN HPE MSA et de 96 disques de 2 TB ce qui permetterait de disposer de 49,152 TB d'espace disque par SAN ^[Voir http://www.raid-calculator.com/default.aspx] en utilisant un RAID 10 ce qui serait amplement suffisent et protégerait les données contre la perte de trois disques pour le stockage des backups ^[Voir https://support.hpe.com/hpsc/doc/public/display?docId=mmr_kc-0112012] un prix maximum de 56000 USD soit environ 52700 CHF sachant qu'il est usuel d'avoir un rabais de la part du fournisseur pour des commandes importantes.

Le coût du matériel se porterait au minimum à 6595 CHF environ ou à 68305 CHF si les 2 SAN et les 96 disques durs sont achetés pour disposer d'une protection maximum.

Au final, le prix des licences et du matériel nécessaire se porterait lors de l'investissement initial à 41024 CHF ou à 102735 CHF selon l'option choisie puis à 24633 CHF les années suivantes.

\newpage

# Tests

## Infrastructure de test

Une machine virtuelle a été mise en place afin de tester Backup Exec avec un système d'exploitation Windows Server 2016.
Une autre machine virtuelle a été mise en place afin de tester StarWind avec un système d'exploitation Windows Server 2016.
Un domaine existant utilisé pour d'autres projets a été utilisé afin de simuler un environnement d'entreprise.
Une machine virtuelle cliente utilisée pour d'autres projets a servi de client test.
Les machine virtuelle BackupExec et StarWind ont été ajoutée au domaine.
Le logiciel Backup Exec 2016 a été installé avec les options par défaut sur le serveur BackupExec.
Le logiciel StarWind Virtual Tape Library a été installé afin de servir de bibliothèque de tape virtuelle sur le serveur.
Une bibliothque VTL a été créée en simulant des lecteurs HP LTO5 sur le serveur StarWind, elle est stockée sur un disque dur virtuel secondaire de la machine virtuelle.
L'initiateur iSCSi a été lancé et la cible pointant vers le serveur StarWind a été connectée avec succès.

![Connexion iSCSI fonctionnelle](Figures\I-BCKP-CMZ-iSCSiConnection.png)

Les drivers HP StorageWorks ont été installés sur le serveur BackupExec et il est possible de constater qu'ils sont correctements reconnus dans le gestionnaire de périphériques.

![Tape détectées](Figures\I-BCKP-CMZ-TapeWorking.png)

\newpage

Le logiciel BackupExec a été configuré afin d'utiliser le deuxième disque comme stockage disque ainsi que les tapes virtuelles comme support de stockage de bande.

![Stockage BackupExec](Figures\I-BCKP-CMZ-TapeDetectedBackupExec.png)

Puis, l'agent BackupExec a été installé sur la machine cliente de test et ajouté dans la liste des clients.

\newpage

Ensuite, une planification de sauvegarde a été créée pour les partages réseaux.

![Planification sauvegarde partages réseaux](Figures\I-BCKP-CMZ-BackupTaskDesktops.png)

\newpage

Puis, une planification de sauvegarde pour les desktops a été créé.

![Planification sauvegarde desktops](Figures\I-BCKP-CMZ-BackupTaskShares.png)

\newpage

La sauvegarde incrémentielle des partages réseaux a été lancée afin de tester si cela fonctionne.

![Sauvegarde partages](Figures\I-BCKP-CMZ-BackupTaskProgressShares.png)

\newpage

La sauvegarde complète des partages réseaux a été lancée afin de tester si cela fonctionne.

![Sauvegarde partages](Figures\I-BCKP-CMZ-BackupTaskProgressShares.png)

\newpage

Le transfert sur tape a été testé avec succès.

\newpage

La sauvegarde des desktops a été lancée afin de tester son fonctionnement.

![Sauvegarde desktops](Figures\I-BCKP-CMZ-BackupTaskProgressDesktop.png)

\newpage

## Tableau de tests

Vous trouverez ci-dessous le tableau de tests effectués.

| Test | Résultat |
| :---: | :-------: |
| Connection àa la cible iSCSI | OK |
| Détection des tapes par BackupExec | OK |
| Création de la planification de la sauvegarde selon la politique | OK |
| Backup incrementiel bijournalier | OK |
| Backup complet hebdomadaire sur disque | OK |
| Backup complet hebdomadaire sur tape | OK |
| Backup complet mensuel sur disque | OK |
| Backup complet mensuel sur tape | OK |
| Backup complet annuel sur disque | OK |
| Backup complet annuel sur disque | OK |

## Bilan des tests

\newpage

# Conclusion

En conclusion, l'outil de backup Backup exec est un outil utilisé de manière usuelle dans l'industrie.

\newpage

# Références

## Bibliographie

Aucun ouvrage n'a été consulté durant la rédaction du présent document.

## Webographie

Vous trouverez la liste des sites web consultés durant la rédaction du document.

https://en.wikipedia.org/wiki/Backup_Exec, consulté le 31 janvier 2018

https://www.veritas.com/product/backup-and-recovery/backup-exec, consulté le 31 janvier 2018

https://www.veritas.com/content/support/en_US/doc/59226269-99535599-0/v59899992-99535599, consulté le 31 janvier 2018

https://www.veritas.com/content/support/en_US/doc/59226809-128692237-1, consulté le 31 janvier 2018

https://www.veritas.com/support/en_US/article.100012956, consulté le 31 janvier 2018

https://www.starwindsoftware.com/resource-library/starwind-virtual-san-virtual-tape-library-vtl-symantec-backup-exec, consulté le 31 janvier 2018

https://www.cristalink.com/fs/, consulté le 31 janvier 2018

http://wintelguy.com/backupcalc.pl, consulté le 31 janvier 2018

http://blog.open-e.com/d2d2t-a-disk-to-disk-to-tape-backup-strategy/, consulté le 28 février 2018

\newpage

# Annexes

## Schéma réseau de test

Vous trouverez ci-dessous le schéma réseau de l'infrastructure de test.

![Schéma réseau de test](..\Figures\I-BCKP-CMZ-SchemaReseauTest.png)

\newpage

## Schéma réseau planifié

Vous trouverez ci-dessous le schéma réseau de l'infrastructure envisagée.

![Schéma réseau envisagé](..\Figures\I-BCKP-CMZ-SchemaReseau.png)

\newpage

## Flux de sauvegarde

Vous trouverez ci-dessous le flux de sauvegarde envisagé.

![Flux de sauvegarde](..\Figures\I-BCKP-CMZ-FluxSauvegarde.png)
